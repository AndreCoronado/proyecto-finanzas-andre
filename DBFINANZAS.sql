USE [DBFINANZAS]
GO
/****** Object:  Table [dbo].[CuentaCorriente]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CuentaCorriente](
	[CuentaCorrienteId] [int] IDENTITY(1,1) NOT NULL,
	[NroCuenta] [int] NOT NULL,
	[Saldo] [decimal](18, 2) NOT NULL,
	[UsuarioId] [int] NOT NULL,
 CONSTRAINT [PK_CuentaCorriente] PRIMARY KEY CLUSTERED 
(
	[CuentaCorrienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Deposito]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deposito](
	[DepositoId] [int] IDENTITY(1,1) NOT NULL,
	[MontoDepositado] [decimal](18, 2) NOT NULL,
	[NroDiasPlazoFijo] [int] NOT NULL,
	[FechaDeposito] [date] NOT NULL,
	[FechaCancelacion] [date] NULL,
	[FechaVencimiento] [date] NULL,
	[Penalizacion] [decimal](18, 2) NOT NULL,
	[CuentaCorrienteId] [int] NOT NULL,
	[TasaId] [int] NOT NULL,
 CONSTRAINT [PK_Deposito] PRIMARY KEY CLUSTERED 
(
	[DepositoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogUsuario]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogUsuario](
	[UsuarioId] [int] NOT NULL,
	[Codigo] [varchar](20) NOT NULL,
	[Password] [varchar](20) NOT NULL,
	[Bloqueado] [bit] NOT NULL,
 CONSTRAINT [PK_LogUsuario] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[RolId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Descripcion] [text] NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[RolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tasa]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasa](
	[TasaId] [int] IDENTITY(1,1) NOT NULL,
	[ValorTasa] [decimal](18, 2) NOT NULL,
	[Descripcion] [text] NULL,
 CONSTRAINT [PK_Tasa] PRIMARY KEY CLUSTERED 
(
	[TasaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tasa_X_Deposito]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasa_X_Deposito](
	[TasaId] [int] NOT NULL,
	[DepositoId] [int] NOT NULL,
	[ValorHistoricoTasa] [decimal](18, 2) NOT NULL,
	[MontoEsperado] [decimal](18, 2) NULL,
	[FechaAplicada] [date] NOT NULL,
 CONSTRAINT [PK_Tasa_X_Deposito] PRIMARY KEY CLUSTERED 
(
	[TasaId] ASC,
	[DepositoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 27/10/2015 8:53:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[UsuarioId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](40) NOT NULL,
	[Apellido] [varchar](40) NOT NULL,
	[Dni] [char](8) NOT NULL,
	[Ruc] [char](11) NOT NULL,
	[Email] [varchar](40) NOT NULL,
	[RolId] [int] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CuentaCorriente]  WITH CHECK ADD  CONSTRAINT [FK_CuentaCorriente_Usuario] FOREIGN KEY([UsuarioId])
REFERENCES [dbo].[Usuario] ([UsuarioId])
GO
ALTER TABLE [dbo].[CuentaCorriente] CHECK CONSTRAINT [FK_CuentaCorriente_Usuario]
GO
ALTER TABLE [dbo].[Deposito]  WITH CHECK ADD  CONSTRAINT [FK_Deposito_CuentaCorriente] FOREIGN KEY([CuentaCorrienteId])
REFERENCES [dbo].[CuentaCorriente] ([CuentaCorrienteId])
GO
ALTER TABLE [dbo].[Deposito] CHECK CONSTRAINT [FK_Deposito_CuentaCorriente]
GO
ALTER TABLE [dbo].[Tasa_X_Deposito]  WITH CHECK ADD  CONSTRAINT [FK_Tasa_X_Deposito_Deposito] FOREIGN KEY([DepositoId])
REFERENCES [dbo].[Deposito] ([DepositoId])
GO
ALTER TABLE [dbo].[Tasa_X_Deposito] CHECK CONSTRAINT [FK_Tasa_X_Deposito_Deposito]
GO
ALTER TABLE [dbo].[Tasa_X_Deposito]  WITH CHECK ADD  CONSTRAINT [FK_Tasa_X_Deposito_Tasa] FOREIGN KEY([TasaId])
REFERENCES [dbo].[Tasa] ([TasaId])
GO
ALTER TABLE [dbo].[Tasa_X_Deposito] CHECK CONSTRAINT [FK_Tasa_X_Deposito_Tasa]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol] FOREIGN KEY([RolId])
REFERENCES [dbo].[Rol] ([RolId])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Rol]
GO
