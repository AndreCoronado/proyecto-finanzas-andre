﻿using System.Web;
using System.Web.Optimization;

namespace PROYECTO_FINANZAS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                "~/Content/js/jquery.js",
                "~/Content/js/jquery-1.10.2.js",
                "~/Scripts/bootstrap.js",
                "~/Content/js/jquery.metisMenu.js",
                "~/Content/js/custom/tableFilter.js",
                "~/Content/js/morris/raphael-2.1.0.js",
                "~/Content/js/custom.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/css/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/js/morris/morris-0.4.3.css",
                      "~/Content/css/custom.css",
                      "~/Content/css/chart.css"));
        }
    }
}
